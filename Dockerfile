FROM fellipevidal/node-env

RUN npm install --global @nestjs/cli

WORKDIR /usr/src/app

CMD [ "zsh" ]