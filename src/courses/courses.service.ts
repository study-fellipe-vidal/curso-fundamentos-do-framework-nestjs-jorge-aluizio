// Dependencies
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

// DTO's
import { CreateCourseDto } from './dto/create-course-dto';
import { UpdateCourseDto } from './dto/update-course-dto';

// Entities
import { Course } from './entities/course.entity';
import { Tag } from './entities/tag.entity';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly courseRepository: Repository<Course>,

    @InjectRepository(Tag)
    private readonly tagRepository: Repository<Tag>,
  ) {}

  findAll() {
    return this.courseRepository.find({ relations: ['tags'] });
  }

  findOneById(id: string) {
    const course = this.courseRepository.findOne(id, { relations: ['tags'] });

    if (!course) throw new NotFoundException(`Course ID ${id} Not Found`);
    return course;
  }

  async create(createCourseDTO: CreateCourseDto) {
    const tags = await Promise.all(
      createCourseDTO.tags.map((name) => this.preloadTagByName(name)),
    );
    const course = this.courseRepository.create({ ...createCourseDTO, tags });
    return this.courseRepository.save(course);
  }

  async update(id: string, updateCourseDTO: UpdateCourseDto) {
    const tags =
      updateCourseDTO.tags &&
      (await Promise.all(
        updateCourseDTO.tags.map((name) => this.preloadTagByName(name)),
      ));
    const course = await this.courseRepository.preload({
      id: id,
      ...updateCourseDTO,
      tags,
    });
    if (!course) throw new NotFoundException(`Course ID ${id} Not Found`);

    return this.courseRepository.save(course);
  }

  async remove(id: string) {
    const course = await this.courseRepository.findOne(id);
    if (!course) throw new NotFoundException(`Course ID ${id} Not Found`);

    return this.courseRepository.remove(course);
  }

  private async preloadTagByName(name: string) {
    const tag = await this.tagRepository.findOne({ name });
    if (tag) return tag;

    return this.tagRepository.create({ name });
  }
}
